#pragma once

#include <string_view>

namespace libb
{
    void sayGoodbye(std::string_view name);
}
