cmake_minimum_required(VERSION 3.11)
project(libb LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(FETCHCONTENT_UPDATES_DISCONNECTED "ON" CACHE BOOL "Disable/enable the update stage")

include(./cmake/FetchContentHelper.cmake)
FetchContentHelper(libc
    GIT
        git@gitlab.com:test530/libc.git
        origin/master

    ADD_SUBDIR
)

add_library(${PROJECT_NAME} ./src/libb.cpp)
target_include_directories(${PROJECT_NAME} PUBLIC ./include)
target_link_libraries(${PROJECT_NAME} libc)
