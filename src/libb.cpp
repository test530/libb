#include <string>

#include <libc/libc.h>
#include <libb/libb.h>

using namespace std::string_literals;

void libb::sayGoodbye(std::string_view name)
{
    auto message = "Goodbye "s;
    message += name;
    message += '\n';
    libc::print(message);
}
